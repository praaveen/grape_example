module API
  module V1
    class Hussars < Grape::API
      include API::V1::Defaults
      version 'v1' # path-based versioning by default
      format :json # We don't like xml anymore

      resource :hussars do
        desc "Return list of hussars"
        get do
          b= "v1testhu" # obviously you never want to call #all here
        end
      end
    end
  end
end
